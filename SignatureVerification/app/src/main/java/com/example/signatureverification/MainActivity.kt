package com.example.signatureverification

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.signatureverification.databinding.ActivityMainBinding
import com.example.signatureverification.fragments.AuthenticationFragment
import com.example.signatureverification.fragments.HelpFragment
import com.example.signatureverification.fragments.RegistrationFragment

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    private val settingsFragment = RegistrationFragment()
    private val authenticationFragment = AuthenticationFragment()
    private val helpFragment = HelpFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        makeCurrentFragment(settingsFragment)
        binding.bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.ic_settings -> makeCurrentFragment(settingsFragment)
                R.id.ic_authentication -> makeCurrentFragment(authenticationFragment)
                R.id.ic_help -> makeCurrentFragment(helpFragment)
            }
            true
        }
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentHolder, fragment)
            commit()
        }
}