package com.example.signatureverification

object Constants {
    const val MIN_COUNT = 5
    const val LENGTH = 512
}