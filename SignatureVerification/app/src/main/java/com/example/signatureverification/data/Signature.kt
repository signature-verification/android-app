package com.example.signatureverification.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "signatures")
data class Signature(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var x: List<Double>,
    var y: List<Double>
)
