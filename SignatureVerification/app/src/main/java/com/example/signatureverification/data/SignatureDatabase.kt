package com.example.signatureverification.data

import android.content.Context
import androidx.room.*

@Database(entities = [Signature::class], version = 1, exportSchema = false)
@TypeConverters(DataConverter::class)
abstract class SignatureDatabase: RoomDatabase() {

    abstract fun signatureDao(): SignatureDao

    companion object{
        @Volatile
        private var INSTANCE: SignatureDatabase? = null

        fun getSignatureDatabase(context: Context): SignatureDatabase {
            val tempInstance = INSTANCE
            if(tempInstance != null){
                return tempInstance
            }

            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    SignatureDatabase::class.java,
                    "signatures"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}