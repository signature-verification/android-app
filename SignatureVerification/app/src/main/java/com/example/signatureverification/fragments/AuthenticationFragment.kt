package com.example.signatureverification.fragments

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.signatureverification.Constants
import com.example.signatureverification.R
import com.example.signatureverification.data.SignatureViewModel
import com.example.signatureverification.databinding.FragmentAuthenticationBinding
import com.example.signatureverification.models.MachineLearningModel
import com.example.signatureverification.models.TemplateModel
import com.example.signatureverification.utility.Preprocessor

class AuthenticationFragment : Fragment() {
    lateinit var binding: FragmentAuthenticationBinding

    private val inputFragment = InputFragment()
    private val signatureViewModel by lazy { activity?.application?.let { SignatureViewModel(it) } }
    private var templateModel: TemplateModel? = null
    private var machineLearningModel: MachineLearningModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_authentication, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentAuthenticationBinding.bind(view)
        val preprocessor = Preprocessor()

        childFragmentManager.beginTransaction().apply {
            replace(R.id.inputFragmentHolder, inputFragment)
            commit()
        }

        binding.discardButton2.setOnClickListener{
            val data = inputFragment.getData()
            if (data.x.isNotEmpty()) {
                binding.discardButton2.playAnimation()
                inputFragment.deleteCanvas()
            }
        }

        signatureViewModel?.getCount()?.observe(this, {
            if (it >= Constants.MIN_COUNT){
                signatureViewModel?.getSignatures()?.observe(this, { it1 -> templateModel = TemplateModel(it1)
                })
                signatureViewModel?.getSignatures()?.observe(this, { it1 -> machineLearningModel =
                    context?.let { it2 -> MachineLearningModel(it1, it2) }
                })
            } else {
                templateModel = null
                machineLearningModel = null
                AlertDialog.Builder(context)
                    .setTitle("Notification")
                    .setMessage("Please complete the registration first!")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setNeutralButton("Close", null).show()
            }
        })

        binding.addButton2.setOnClickListener {
            if (templateModel != null && machineLearningModel != null) {
                val data = inputFragment.getData()
                if(data.x.isNotEmpty()){
                    var k = 1.0
                    if (binding.editTextNumber.text.toString().isNotBlank()){
                        k = binding.editTextNumber.text.toString().toDouble()
                    }

                    val processedData = preprocessor.processing(data)

                    if (templateModel?.validate(processedData, k) == true) {
                        AlertDialog.Builder(context)
                            .setTitle("TM Result")
                            .setMessage("Success!")
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setNeutralButton("Close", null).show()
                    } else {
                        AlertDialog.Builder(context)
                            .setTitle("TM Result")
                            .setMessage("Refused!")
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setNeutralButton("Close", null).show()
                    }

                    if (machineLearningModel?.validate(processedData, k) == true) {
                        AlertDialog.Builder(context)
                            .setTitle("ML Result")
                            .setMessage("Success!")
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setNeutralButton("Close", null).show()
                    } else {
                        AlertDialog.Builder(context)
                            .setTitle("ML Result")
                            .setMessage("Refused!")
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setNeutralButton("Close", null).show()
                    }

                    inputFragment.deleteCanvas()
                }
            }
        }
    }
}