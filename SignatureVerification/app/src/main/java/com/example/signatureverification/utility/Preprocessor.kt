package com.example.signatureverification.utility

import com.example.signatureverification.Constants
import com.example.signatureverification.data.Signature
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator
import kotlin.math.pow

class Preprocessor {

    fun processing(signature: Signature): Signature{
        var processedSignature = interpolation(signature)
        processedSignature = zScoreNormalization(processedSignature)
        processedSignature = diff(processedSignature)
        return processedSignature
    }

    private fun zScoreNormalization(signature: Signature): Signature{
        val x = signature.x.toMutableList()
        val y = signature.y.toMutableList()

        var mu1 = 0.0
        var mu2 = 0.0

        var sigma1 = 0.0
        var sigma2 = 0.0

        val n = x.size

        for (i in 0 until n){
            mu1 += x[i]
            mu2 += y[i]
        }

        mu1 /= n
        mu2 /= n

        for (i in 0 until n){
            sigma1 += (x[i] - mu1).pow(2)
            sigma2 += (y[i] - mu2).pow(2)
        }

        sigma1 = (sigma1/n).pow(0.5)
        sigma2 = (sigma2/n).pow(0.5)

        for (i in 0 until n){
            x[i] = (x[i] - mu1)/sigma1
            y[i] = (y[i] - mu2)/sigma2
        }

        signature.x = x
        signature.y = y

        return signature
    }

    private fun diff(signature: Signature): Signature{
        val n = signature.x.size

        val x = signature.x.toDoubleArray()
        val y = signature.y.toDoubleArray()

        for(i in n-1 downTo 1){
            x[i] -= x[i-1]
            y[i] -= y[i-1]
        }
        x[0] = 0.toDouble()
        y[0] = 0.toDouble()

        signature.x = x.toList()
        signature.y = y.toList()

        return signature
    }

    private fun interpolation(signature: Signature): Signature{
        val x = signature.x
        val y = signature.y

        val n = x.size

        val t = arangeD(0.0, n.toDouble(), 1.0)
        val dt = t[n-1]/ Constants.LENGTH
        val tnew = arangeD(0.0, t[n-1], dt)

        val si = SplineInterpolator()

        val fx = si.interpolate(t.toDoubleArray(), x.toDoubleArray())
        val fy = si.interpolate(t.toDoubleArray(), y.toDoubleArray())

        val xnew = mutableListOf<Double>()
        val ynew = mutableListOf<Double>()

        for(time in tnew){
            xnew.add(fx.value(time))
            ynew.add(fy.value(time))
        }

        signature.x = xnew
        signature.y = ynew

        return signature
    }

    private fun arangeD(start: Double, stop: Double, step: Double): MutableList<Double> {
        val result = mutableListOf<Double>()
        var s = start

        result.add(s)

        while (s+step < stop){
            s += step
            result.add(s)
        }

        return result
    }
}