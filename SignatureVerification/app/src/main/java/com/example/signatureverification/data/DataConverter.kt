package com.example.signatureverification.data

import androidx.room.TypeConverter

class DataConverter {
    @TypeConverter
    fun fromListOfFloats(list: List<Double>?): String {
        return list?.joinToString(separator = ";") { it.toString() } ?: ""
    }

    @TypeConverter
    fun toListOfFloats(string: String?): List<Double> {
        return string?.split(";")?.mapNotNull { it.toDoubleOrNull() } ?: emptyList()
    }

}