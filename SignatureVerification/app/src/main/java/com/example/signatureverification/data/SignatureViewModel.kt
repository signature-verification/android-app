package com.example.signatureverification.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SignatureViewModel(application: Application): AndroidViewModel(application) {

    private val repository: SignatureRepository

    init{
        val signatureDao = SignatureDatabase.getSignatureDatabase(application).signatureDao()
        repository = SignatureRepository(signatureDao)
    }

    fun addSignature(signature: Signature){
        viewModelScope.launch(Dispatchers.IO){
            repository.addSignature(signature)
        }
    }

    fun getSignatures(): LiveData<List<Signature>>{
        return repository.signatures
    }

    fun resetSignatures(){
        viewModelScope.launch(Dispatchers.IO) {
            repository.resetSignatures()
        }
    }

    fun getCount(): LiveData<Int>{
        return repository.count
    }
}