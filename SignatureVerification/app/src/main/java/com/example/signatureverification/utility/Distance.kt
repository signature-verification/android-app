package com.example.signatureverification.utility

import android.util.Log
import com.example.signatureverification.data.Signature
import java.lang.Math.abs

class Distance {
    private fun getDistance(s1: Signature, s2: Signature): Double{
        val n = s1.x.size

        var distance = 0.0

        for(i in 0 until n){
            distance += abs(s1.x[i] - s2.x[i]) + abs(s1.y[i] - s2.y[i])
        }

        return distance
    }

    @JvmName("getDistance1")
    fun getDistance(signatureList: List<Signature>): List<Double>{
        val distances = mutableListOf<Double>()

        for(i in 0 until (signatureList.size-1)){
            for (j in i+1 until signatureList.size){
                distances.add(getDistance(signatureList[i], signatureList[j]))
            }
        }

        return distances
    }

    fun getDistance(s: Signature, signatureList: List<Signature>): List<Double>{
        val distances = mutableListOf<Double>()

        for(sig in signatureList){
            distances.add(getDistance(s, sig))
        }

        return distances
    }

    private fun getDistance(f1: List<Float>, f2:List<Float>): Float{
        val n = f1.size

        var distance = 0.0.toFloat()

        for(i in 0 until n){
            distance += abs(f1[i] - f2[i])
        }

        return distance
    }

    fun getDistance(f: List<Float>, features: List<List<Float>>): List<Float>{
        val distances = mutableListOf<Float>()

        for(sig in features){
            distances.add(getDistance(f, sig))
        }

        return distances
    }

    fun getDistance(features: List<List<Float>>): List<Float>{
        val distances = mutableListOf<Float>()

        for(i in 0 until (features.size-1)){
            for (j in i+1 until features.size){
                distances.add(getDistance(features[i], features[j]))
            }
        }

        return distances
    }

}