package com.example.signatureverification.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface SignatureDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addSignature(signature: Signature)

    @Query("SELECT * FROM signatures")
    fun getSignatures(): LiveData<List<Signature>>

    @Query("DELETE FROM signatures")
    suspend fun resetSignatures()

    @Query("SELECT COUNT(id) FROM signatures")
    fun getCount(): LiveData<Int>

}