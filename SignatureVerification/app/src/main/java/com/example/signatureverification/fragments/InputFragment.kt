package com.example.signatureverification.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.signatureverification.InputCanvas
import com.example.signatureverification.R
import com.example.signatureverification.data.Signature

class InputFragment : Fragment() {
    private lateinit var inputCanvas: InputCanvas

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        inputCanvas = InputCanvas(context)
        return inputCanvas
    }

    fun deleteCanvas(){
        inputCanvas.deleteCanvas()
    }

    fun getData(): Signature {
        return inputCanvas.getData()
    }
}