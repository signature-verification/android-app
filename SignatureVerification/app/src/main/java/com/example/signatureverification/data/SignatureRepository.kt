package com.example.signatureverification.data

import androidx.lifecycle.LiveData

class SignatureRepository(private val signatureDao: SignatureDao) {

    val signatures: LiveData<List<Signature>> = signatureDao.getSignatures()
    val count: LiveData<Int> = signatureDao.getCount()

    suspend fun addSignature(signature: Signature){
        signatureDao.addSignature(signature)
    }

    suspend fun resetSignatures(){
        signatureDao.resetSignatures()
    }

}