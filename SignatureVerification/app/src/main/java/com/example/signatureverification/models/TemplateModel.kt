package com.example.signatureverification.models

import android.util.Log
import com.example.signatureverification.data.Signature
import com.example.signatureverification.utility.Distance
import kotlin.math.pow
import kotlin.math.sqrt


class TemplateModel(signatureList: List<Signature>) {
    private var mean = 0.0
    private var std = 0.0
    private val signatureList: List<Signature> = signatureList

    init {
        val distances = Distance().getDistance(signatureList)

        for (d in distances){
            mean += d
        }

        mean /= distances.size

        for(d in distances){
            std +=  (d - mean).pow(2)
        }

        std /= distances.size
        std = sqrt(std)
    }

    fun validate(s: Signature, k: Double): Boolean{
        var avg = 0.0
        val d = Distance().getDistance(s, signatureList)

        for (distance in d){
            avg += distance
        }

        avg /= signatureList.size

//        Log.d("DISTANCE", avg.toString())
//        Log.d("IN RANGE", "(" + (mean-k*std).toString() + ", " + (mean+k*std).toString() + ")")

        return avg <= mean+k*std && avg >= mean-k*std
    }
}