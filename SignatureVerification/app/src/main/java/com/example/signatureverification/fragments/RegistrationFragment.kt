package com.example.signatureverification.fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.signatureverification.Constants
import com.example.signatureverification.R
import com.example.signatureverification.data.SignatureViewModel
import com.example.signatureverification.databinding.FragmentRegistrationBinding
import com.example.signatureverification.utility.Preprocessor


class RegistrationFragment : Fragment() {
    lateinit var binding: FragmentRegistrationBinding

    private val inputFragment = InputFragment()
    private val signatureViewModel by lazy { activity?.application?.let { SignatureViewModel(it) } }
    private var signatureCount = 0
    private var flag = 0


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onResume() {
        super.onResume()
        flag = 0
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentRegistrationBinding.bind(view)
        val preprocessor = Preprocessor()

        childFragmentManager.beginTransaction().apply {
            replace(R.id.inputFragmentHolder, inputFragment)
            commit()
        }

        signatureViewModel?.getCount()?.observe(this, {
            if (it != null) {
                val count = it.toString() + "/" + Constants.MIN_COUNT
                binding.countTextView.text = count

                signatureCount = it

                if (it == Constants.MIN_COUNT){
                    AlertDialog.Builder(context)
                        .setTitle("Notification")
                        .setMessage("The registration has ended!")
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setNeutralButton("Close", null).show()
                }

                if (signatureCount < Constants.MIN_COUNT && flag == 0){
                    AlertDialog.Builder(context)
                        .setTitle("Notification")
                        .setMessage("Draw your signature on the grey area at least " + Constants.MIN_COUNT + " times!")
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setNeutralButton("Close", null).show()
                    flag = 1
                }
            }
        })


        binding.resetButton.setOnClickListener{
            AlertDialog.Builder(context)
                    .setTitle("Confirm")
                    .setMessage("Do you really want to delete all your signatures?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton("Yes") { _: DialogInterface, _: Int -> signatureViewModel?.resetSignatures() }
                    .setNegativeButton("No", null).show()
        }

        binding.addButton.setOnClickListener {
            if (signatureCount >= Constants.MIN_COUNT){
                AlertDialog.Builder(context)
                    .setTitle("Notification")
                    .setMessage("The registration has ended!")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setNeutralButton("Close", null).show()
            }
            else{
                val data = inputFragment.getData()
                if (data.x.isNotEmpty()) {
                    binding.addButton.progress = 0f
                    binding.addButton.playAnimation()

                    signatureViewModel?.addSignature(preprocessor.processing(data))

                    inputFragment.deleteCanvas()
                }
            }
        }

        binding.discardButton.setOnClickListener{
            val data = inputFragment.getData()
            if (data.x.isNotEmpty()) {
                binding.discardButton.playAnimation()
                inputFragment.deleteCanvas()
            }
        }
    }
}