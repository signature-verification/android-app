package com.example.signatureverification

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.example.signatureverification.data.Signature

class InputCanvas(context: Context?) : View(context) {
    private val paint= Paint()
    private val path = Path()

    private var x = mutableListOf<Double>()
    private var y = mutableListOf<Double>()

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawRGB(220, 220, 220)
        canvas.drawPath(path, paint)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val xPos = event.x
        val yPos = event.y
        //val pressure = event.pressure

        when(event.action){
            MotionEvent.ACTION_DOWN -> {
                path.moveTo(xPos, yPos)
                x.add(xPos.toDouble())
                y.add(yPos.toDouble())
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                path.lineTo(xPos, yPos)
                x.add(xPos.toDouble())
                y.add(yPos.toDouble())
            }
            MotionEvent.ACTION_UP -> {
            }
            else -> return false
        }
        invalidate()
        return true
    }

    init {
        paint.isAntiAlias = true
        paint.color = Color.BLACK
        paint.strokeJoin = Paint.Join.ROUND
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 5f
    }

    fun deleteCanvas(){
        path.reset()
        invalidate()
        x.clear()
        y.clear()
    }

    fun getData(): Signature {
        return Signature(0, x, y)
    }
}