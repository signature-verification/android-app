package com.example.signatureverification.models

import android.content.Context
import android.content.res.AssetFileDescriptor
import android.content.res.AssetManager
import android.util.Log
import com.example.signatureverification.data.Signature
import com.example.signatureverification.utility.Distance
import org.tensorflow.lite.Interpreter
import java.io.FileInputStream
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import kotlin.math.pow
import kotlin.math.sqrt

@Suppress("DEPRECATION")
class MachineLearningModel(private val signatureList: List<Signature>, private var context: Context) {
    private var mean = 0.0.toFloat()
    private var std = 0.0.toFloat()
    private var extractedFeatureList = mutableListOf<List<Float>>()

    private val interpreter by lazy {
        Interpreter(loadModelFile())
    }

    private fun loadModelFile(): MappedByteBuffer {
        val fileDescriptor: AssetFileDescriptor = context.assets.openFd("model.tflite")
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel: FileChannel = inputStream.channel
        val startOffset: Long = fileDescriptor.startOffset
        val declaredLength: Long = fileDescriptor.declaredLength
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    private fun inference(input: Array<FloatArray>): Array<FloatArray> {
         val output = Array(1){FloatArray(256)}

        interpreter.run(arrayOf(input), output)

        return output
    }

    init {
        // Converting signatures to Float array
        val convertedSignatures = mutableListOf<Array<FloatArray>>()

        for (s in signatureList){
            convertedSignatures.add(convert(s))
        }

        // Extracting features from signatures
        for (c in convertedSignatures){
            extractedFeatureList.add(inference(c)[0].toList())
        }


        val distances = Distance().getDistance(extractedFeatureList)

        for (d in distances){
            Log.d("ML DISTANCES", d.toString())
            mean += d
        }

        mean /= distances.size

        for(d in distances){
            std +=  (d - mean).pow(2)
        }

        std /= distances.size
        std = sqrt(std)

        Log.d("MEAN", mean.toString())
        Log.d("STD", std.toString())
    }

    fun validate(s: Signature, k: Double): Boolean{
        var avg = 0.0.toFloat()
        val features = inference(convert(s))[0].toList()

        val d = Distance().getDistance(features, extractedFeatureList)

        //Log.d("DISTANCES", Distance().getDistance(features, extractedFeatureList[0]).toString())

        for (distance in d){
            avg += distance
        }

        avg /= extractedFeatureList.size

        Log.d("DISTANCE", avg.toString())
        Log.d("IN RANGE", "(" + (mean-k*std).toString() + ", " + (mean+k*std).toString() + ")")

        return avg <= mean+k*std && avg >= mean-k*std
    }

    private fun convert(signature: Signature): Array<FloatArray>{
        val xArray = FloatArray(512)
        val yArray = FloatArray(512)

        for(i in 0 until 511) {
            xArray[i] = signature.x[i].toFloat()
            yArray[i] = signature.y[i].toFloat()
        }

        val floatArray = arrayOf(xArray, yArray)

        // Transpose the matrix
        val transposed = Array(512) { FloatArray(2) }
        for (i in 0 until 1) {
            for (j in 0 until 511) {
                transposed[j][i] = floatArray[i][j]
            }
        }

        return transposed
    }
}